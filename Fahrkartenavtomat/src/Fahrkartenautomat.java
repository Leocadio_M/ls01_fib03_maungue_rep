﻿import java.util.Scanner;
class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	boolean b = true;
    	boolean c;
    	Scanner tastatur = new Scanner(System.in);
    	String entscheidung;
    	while(b) {
    		c = true;
    		// Den zu zahlenden Betrag ermittelt normalerweise der Automat
    		// aufgrund der gewählten Fahrkarte(n).
    		// -----------------------------------
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
    		// Geldeinwurf
    		// -----------
    		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

    		// Fahrscheinausgabe
    		// -----------------
    		fahrkartenAusgeben();

    		// Rückgeldberechnung und -Ausgabe
    		// -------------------------------
    		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    							"vor Fahrtantritt entwerten zu lassen!\n"+
                          		"Wir wünschen Ihnen eine gute Fahrt.");
    		System.out.println("Wollen Sie eine Weitere Fahrkarte kaufen?\n"+
                          		"(j) für JA\n"+
    							"(n) für NEIN");
    		while(c) {
    			entscheidung = tastatur.next();
    			if(entscheidung.equals("j")) 
    				c = false;
    			else if(entscheidung.equals("n")) {
    				b = false;
    				c = false;
    			} else
    				System.out.println("Eingabe Falsch!\n(j) für JA\n(n) für NEIN!!!!");
    		}
    	}
    }
    
    //Ermittelt Fahrkartenbestellung
    //Return double zuZahlenderBetrag - Betrag der zurückgegeben wird
    public static double fahrkartenbestellungErfassen() {
    	boolean b;
    	boolean a = true;
    	Scanner tastatur = new Scanner(System.in);
    	int anzahlTickets;
    	int entscheidung;
    	double zuZahlenderBetrag = 0.0;
    	
    	double einzelfahrausweis = 1.2;
    	double tageskarte = 2.3;
    	double kleingruppenTageskarte = 8.9;
    	
    	
    	final double preise[] = {2.9,3.3,3.6,1.9,8.6,9.0,9.6,23.5,24.3,24.9};
    	
    	final String[] fahrkartennamen = {"Einzelfahrschein Berlin AB",
		 		  "Einzelfahrschein Berlin BC",
		 		  "Einzelfahrschein Berlin ABC",
		 		  "Kurzstecke",
		 		  "Tageskarte Berlin AB",
		 		  "Tageskarte Berlin BC",
		 		  "Tageskarte Berlin ABC",
		 		  "Kleingruppen-Tageskarte Berlin AB",
		 		  "Kleingruppen-Tageskarte Berlin BC",
		 		  "Kleingruppen-Tageskarte Berlin ABC"};
    	
    	
    	
    	while(a) {
    		b = true;
    		for(int i=0;i<preise.length;i++) {
    			System.out.println("("+(i+1)+") "+preise[i] + " " + fahrkartennamen[i]);
    		}
    		System.out.println("(0) - Bezahlen");
    		System.out.print("Ihre Wahl: ");
    		entscheidung = tastatur.nextInt();
    		if(entscheidung > 10 || entscheidung < 0) {
    			System.out.println("Sie haben sich falsch entschieden!\nBitte wiederholen Sie die Eingabe!");
    			warte(400);
    			System.out.println("");
    			continue;
    		}
    		if (entscheidung == 0) {
    			a = false;
    			if (zuZahlenderBetrag == 0.0) {
    				System.out.println("Kein Ticket gewählt, Auf Wiedersehen!");
    			}
    			return zuZahlenderBetrag;
    		}
			System.out.print("\nWie viele Fahrkarten möchten Sie kaufen? ");
			anzahlTickets = tastatur.nextInt();
			System.out.println("Anzahl der Tickets: "+anzahlTickets);
                
    		while(b) {
    			if(anzahlTickets <= 0) {
    				System.out.println("Es muss mindestens 1 Ticket\n"+
    									"erworben werden!");
    				b = false;
    				warte(400);
    				System.out.println("");
    			}else if(anzahlTickets > 10) {
    				System.out.println("Es dürfen maximal 10 Tickets\n"+
    									"erworben werden!");
    				b = false;
    				warte(400);
    				System.out.println("");
    			}else {
    				if(entscheidung == 1) {
    					zuZahlenderBetrag = preise[0]*anzahlTickets;
    				} else if(entscheidung == 2) {
    					zuZahlenderBetrag = preise[1]*anzahlTickets;
    				}else if(entscheidung == 3) {
    					zuZahlenderBetrag = preise[2]*anzahlTickets;
    				}else if(entscheidung == 4) {
    					zuZahlenderBetrag = preise[3]*anzahlTickets;
    				}else if(entscheidung == 5) {
    					zuZahlenderBetrag = preise[4]*anzahlTickets;
    				}else if(entscheidung == 6) {
    					zuZahlenderBetrag = preise[5]*anzahlTickets;
    				}else if(entscheidung == 7) {
    					zuZahlenderBetrag = preise[6]*anzahlTickets;
    				}else if(entscheidung == 8) {
    					zuZahlenderBetrag = preise[7]*anzahlTickets;
    				}else if(entscheidung == 9) {
    					zuZahlenderBetrag = preise[8]*anzahlTickets;
    				}else if(entscheidung == 10) {
    					zuZahlenderBetrag = preise[9]*anzahlTickets;
    				}else if(entscheidung == 0) {
    					a = false;
    	    			if (zuZahlenderBetrag == 0.0) {
    	    				System.out.println("Kein Ticket gewählt, Auf Wiedersehen!");
    	    			}
    	    			return zuZahlenderBetrag;
    				}
    				b = false;
    			}
    		}
    	}
    	return zuZahlenderBetrag;
    }
    
    //Bezahlt die Fahrkarte
    //Return double eingezahlterGesamtbetrag - Betrag der insgesamt eingezahlt wurde
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
    		System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   	eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	warte(250);
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	int betrag;
    	rückgabebetrag *= 100;
    	betrag = (int) rückgabebetrag;
    	rückgabebetrag /= 100;
        if(betrag > 0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n" , rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(betrag >= 200) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          betrag -= 200;
            }
            while(betrag >= 100) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          betrag -= 100;
            }
            while(betrag >= 50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          betrag -= 50;
            }
            while(betrag >= 20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          betrag -= 20;
            }
            while(betrag >= 10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          betrag -= 10;
            }
            while(betrag > 0)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          betrag -= 5;
            }
        }
    }
    
    public static void warte(int millisekunden) {
    	for (int i = 0; i < 8; i++)
        {
        	System.out.print("=");
        	try {
        		Thread.sleep(millisekunden);
        	} catch (InterruptedException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
        }
    }
    
    
}