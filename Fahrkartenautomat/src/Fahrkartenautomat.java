﻿import java.util.Scanner;
class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       	Scanner tastatur = new Scanner(System.in);
    	String entscheidung;
    	
    	System.out.println("Fahrkartenbestellvorgang:");
    	System.out.println("=========================\n");
    	
    	while(true) {
    		// Den zu zahlenden Betrag ermittelt normalerweise der Automat
    		// aufgrund der gewählten Fahrkarte(n).
    		// -----------------------------------
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       
    		// Geldeinwurf
    		// -----------
    		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

    		
    		if(zuZahlenderBetrag != 0.0) {
    			// Fahrscheinausgabe
        		// -----------------
        		fahrkartenAusgeben();
        		
        		// Rückgeldberechnung und -Ausgabe
        		// -------------------------------
        		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

        		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
        							"vor Fahrtantritt entwerten zu lassen!\n"+
                              		"Wir wünschen Ihnen eine gute Fahrt.");
    		}else {
    			System.out.println("Kein Ticket gewählt, Auf Wiedersehen!");
				warte(50);
    			System.out.println("");
    		}

    		
    		System.out.println("Wollen Sie eine Weitere Fahrkarte kaufen?\n"+
                          		"(j) für JA\n"+
    							"(n) für NEIN");
    		while(true) {
    			entscheidung = tastatur.next();
    			
    			if(entscheidung.equals("j")) 
    				break;
    			
    			else if(entscheidung.equals("n")) {
    				System.out.println(">>>> Programm Ende <<<<");
    				tastatur.close();
    				System.exit(0);
    			} 
    			else
    				System.out.println("Eingabe Falsch!\n(j) für JA\n(n) für NEIN");
    		}
    	}
    }
    
    /**
     * Ermittelt den zu zahlenden Betrag für den Fahrkartenautomat,
     * und gibt ihn zurück.
     * Enthält zentralen Ort zur Bearbeitung der Fahrkarteninformationen.
     * @return der zu zahlende Betrag
     */
    public static double fahrkartenbestellungErfassen(Scanner tastatur) {
    	int anzahlTickets;
    	int entscheidung;
    	double zuZahlenderBetrag = 0.0;
    	
    	final double fahrkartenpreise[] = {2.9,
    									   3.3,
    									   3.6,
    									   1.9,
    									   8.6,
    									   9.0,
    									   9.6,
    									   23.5,
    									   24.3,
    									   24.9};
    	
    	final String[] fahrkartennamen = {"Einzelfahrschein Berlin AB",
    							 		  "Einzelfahrschein Berlin BC",
    							 		  "Einzelfahrschein Berlin ABC",
    							 		  "Kurzstecke",
    							 		  "Tageskarte Berlin AB",
    							 		  "Tageskarte Berlin BC",
    							 		  "Tageskarte Berlin ABC",
    							 		  "Kleingruppen-Tageskarte Berlin AB",
    							 		  "Kleingruppen-Tageskarte Berlin BC",
    							 		  "Kleingruppen-Tageskarte Berlin ABC"};
    	
    	    
    	while(true) {
    		fahrkartenAnzeigen(fahrkartennamen, fahrkartenpreise);    		
    		entscheidung = ticketWaehlen(fahrkartennamen, tastatur);    
    		
    		if (entscheidung == 0)
    			return zuZahlenderBetrag;
    		
    		else
    			System.out.printf("\t%-36s\n",fahrkartennamen[entscheidung-1]);
    		
    		anzahlTickets = anzahlWaehlen(tastatur);
    		zuZahlenderBetrag += fahrkartenpreise[entscheidung-1]*anzahlTickets;
    	}
    }
    
    /**
     * Gibt die Fahrkartennamen und -preise aus
     * @param namen Array mit Fahrkartennamen
     * @param preise Array mit Fahrkartenpreisen
     */
    public static void fahrkartenAnzeigen(String[] namen, double[] preise) {
    	System.out.println("Wählen Sie:");
    	
		for(int i = 0; i < namen.length; i++) {
			System.out.printf("\t%-36s%6.2f%3s(%d)\n",namen[i],preise[i],"",i+1);
		}
		
		System.out.printf("\t%-45s(0)\n\n","Bezahlen");
    }
    
    /**
     * Liest die Eingabe des Kunden ein und gibt den Wert
     * der Eingabe zurück bezogen auf die entsprechende 
     * Fahrkarte
     * @param namen Array mit den Namen der Fahrkarten
     * @return die Zahl der Fahrkarte für die der Nutzer sich
     * 		   entschieden hat
     */
    public static int ticketWaehlen(String[] namen, Scanner tastatur) {
    	int entscheidung;
    	
    	while(true) {
    		System.out.print("Ihre Wahl: ");
    		entscheidung = tastatur.nextInt();
    		
    		if(entscheidung > 10 || entscheidung < 0) {
    			System.out.println("Sie haben sich falsch entschieden!\n" +
    							   "Bitte wiederholen Sie die Eingabe!");
    			warte(100);
    			continue;
    			
    		}else {
    			return entscheidung;
    		}
    	}
    }
    
    /**
     * Gibt die Anzahl der Fahrkarten zurück die
     * der Kunde kaufen möchte
     * @return Anzahl der Fahrkarten
     */
    public static int anzahlWaehlen(Scanner tastatur) {
    	int anzahl;
    	
    	while(true) {
        	System.out.print("Anzahl Tickets: ");
    		anzahl = tastatur.nextInt();
    		
    		if(anzahl <= 0) {
				System.out.println("Es muss mindestens 1 Ticket\n"+
									"erworben werden!");
				warte(50);
				
			}else if(anzahl > 10) {
				System.out.println("Es dürfen maximal 10 Tickets\n"+
									"erworben werden!");
				warte(50);
				
			}else 
				return anzahl;
    	}
    }
    
    
    /**
     * Bezahlvorgang, Kunde kann Geld einwerfen um Fahrkarte
     * zu bezahlen
     * @param zuZahlenderBetrag der Betrag den der Kunde 
     * 	      noch zu zahlen hat
     * @return gezahlter Gesamtbetrag
     */
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
    		System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   	System.out.print("Eingabe (mind. 5Ct, höchstens 20 Euro): ");
      	   	
      	   	eingeworfeneMünze = tastatur.nextDouble();
      	   	if(pruefeGeld(eingeworfeneMünze)) {
      	   		eingezahlterGesamtbetrag += eingeworfeneMünze;
      	   		
      	   	} else {
      	   		System.out.println("Dieser Betrag ist nicht korrekt.");
      	   		continue;
      	   	}            
        }
    	return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	warte(250);
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	int zaehler=0;
    	String[][] muenzenArray = {{""},{""},{""}};
    	
    	double rueckgabeBetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;    	
    	rueckgabeBetrag = rundeBetrag(rueckgabeBetrag);
    	
    	System.out.printf("Der Rückgabebetrabg in Höhe von %.2f Euro\n" , rueckgabeBetrag);
    	System.out.println("wird in folgenden Münzen ausgezahlt:");
    	
        while(rueckgabeBetrag > 0)
        { 
        	if(zaehler == 3) {
        		muenzeAusgeben(muenzenArray, zaehler);
        		zaehler = 0;
        	}
        	
            if(rueckgabeBetrag >= 2) // 2 EURO-Münzen
            {
            	muenzenArray[zaehler] = erstelleMuenze(2);
            	
    	        rueckgabeBetrag -= 2;
    	        rueckgabeBetrag = rundeBetrag(rueckgabeBetrag);    	        
    	        zaehler++;
    	        
    	        continue;
            }
            if(rueckgabeBetrag >= 1) // 1 EURO-Münzen
            {
            	muenzenArray[zaehler] = erstelleMuenze(1);
            	
    	        rueckgabeBetrag -= 1;
    	        rueckgabeBetrag = rundeBetrag(rueckgabeBetrag);
    	        zaehler++;
    	        
    	        continue;
            }
            if(rueckgabeBetrag >= 0.5) // 50 CENT-Münzen
            {
            	muenzenArray[zaehler] = erstelleMuenze(0.5);
            	
    	        rueckgabeBetrag -= 0.5;
    	        rueckgabeBetrag = rundeBetrag(rueckgabeBetrag);
    	        zaehler++;
    	        
    	        continue;
            }
            if(rueckgabeBetrag >= 0.2) // 20 CENT-Münzen
            {
            	muenzenArray[zaehler] = erstelleMuenze(0.2);
            	
    	        rueckgabeBetrag -= 0.2;
    	        rueckgabeBetrag = rundeBetrag(rueckgabeBetrag);
    	        zaehler++;
    	        
    	        continue;
            }
            if(rueckgabeBetrag >= 0.1) // 10 CENT-Münzen
            {
            	muenzenArray[zaehler] = erstelleMuenze(0.1);
            	
    	        rueckgabeBetrag -= 0.1;
    	        rueckgabeBetrag = rundeBetrag(rueckgabeBetrag);
    	        zaehler++;
    	        
    	        continue;
            }
            if(rueckgabeBetrag > 0)// 5 CENT-Münzen
            {
            	muenzenArray[zaehler] = erstelleMuenze(0.05);
            	
    	        rueckgabeBetrag -= 0.05;
    	        rueckgabeBetrag = rundeBetrag(rueckgabeBetrag);
    	        zaehler++;
    	        
    	        continue;
            }
        }
        muenzeAusgeben(muenzenArray, zaehler);
    }
    
    public static void warte(int millisekunden) {
    	for (int i = 0; i < 8; i++)
        {
        	System.out.print("=");
        	try {
        		Thread.sleep(millisekunden);
        	} catch (InterruptedException e) {
        		e.printStackTrace();
        	}
        }
    	System.out.println("");
    }
        
    /**
     * prüft Münze auf Korrektheit
     * enthält auch 5 - 20 Euro-Scheine wegen
     * Nutzerfreundlichkeit
     * @param muenze der zu prüfende Wert
     * @return ob die Münze korrekt ist
     */
    public static boolean pruefeGeld(double geld) {
    	if(geld == 0.05 || 
    	   geld == 0.1 || 
    	   geld == 0.2 || 
    	   geld == 0.5 || 
    	   geld == 1.0 || 
    	   geld == 2.0 ||
    	   geld == 5.0 ||
    	   geld == 10.0 ||
    	   geld == 20.0) {
    		return true;
    		
    	} else {
    		return false;
    	}
    }
   
    /**
     * Rundet Betrag auf
     * @param betrag der Betrag der gerundet wird
     * @return gerundeter Betrag
     */
   public static double rundeBetrag(double betrag) {
	   betrag *= 100;
	   betrag = Math.rint(betrag);
	   return betrag /= 100;
	   
   }
   
   /**
    * fügt Geldbetrag in die Münzen-Struktur ein
    * @param struktur die Münzen-Struktur
    * @param betrag der einzufügende Betrag
    * @return die Münze mit Betrag
    */
   public static String[] erstelleMuenze(double betrag){
	   String[] struktur = {"    ***    ",
			   				"  *     *  ",
			   				"*         *",
			   				"*         *",
			   				"  *     *  ",
			   				"    ***    "};
	   
	   if(betrag < 1.0) {
		   struktur[3] = "*   CENT  *";
		   
		   if(betrag == 0.05) {
			   struktur[2] = "*    5    *";
			   
		   } else if(betrag == 0.1) {
			   struktur[2] = "*    10   *";
			   
		   } else if(betrag == 0.2) {			   
			   struktur[2] = "*    20   *";
			   
		   } else {
			   struktur[2] = "*    50   *";
			   
		   }
	   } else {
		   struktur[3] = "*   EURO  *";
		   
		   if(betrag == 1.0) {
			   struktur[2] = "*    1    *";
			   
		   } else {
			   struktur[2] = "*    2    *";
		   }
	   }
	   return struktur;
   }
   
   /**
    * gibt die Münzen aus in Std-Out
    * @param muenze array mit maximal 3 Münzen
    * @param zaehler Anzahl der Münzen die ausgegeben werden
    */
   	public static void muenzeAusgeben(String[][] muenze, int zaehler) {
   		switch (zaehler) {
		case 0:	return;
		
		case 1: for(int i=0;i<muenze[0].length;i++) {
					System.out.println(muenze[0][i]);
				}
				break;
				
		case 2: for(int i=0;i<muenze[0].length;i++) {
					System.out.println(muenze[0][i] + " " + 
									   muenze[1][i]);
				}
				break;
				
		case 3: for(int i=0;i<muenze[0].length;i++) {
					System.out.println(muenze[0][i] + " " + 
									   muenze[1][i] + " " + 
									   muenze[2][i]);
				}
				break;
		}
		
   	}
    
}