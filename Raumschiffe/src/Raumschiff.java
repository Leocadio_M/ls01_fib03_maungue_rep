import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

/**
 * Lernsituation Raumschiffe - Klasse Raumschiff
 * @author LeocadioMaungue
 * @version 1.0.0.0
 *
 */
public class Raumschiff {
	private String name;
	private int energieversorgung;
	private int schutzschilde;
	private int lebenserhaltungsysteme;
	private int huelle;
	private int photonentorpedos;
	private int reparaturAndroiden;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();
	
	public Raumschiff() {
	}

	public Raumschiff(String name, 
					  int energieversorgung, 
					  int schutzschilde, 
					  int lebenserhaltungsysteme, 
					  int huelle, 
					  int photonentorpedos, 
					  int reparaturAndroiden, 
					  ArrayList<String> broadcastKommunikator) {
		this.name = name;
		this.energieversorgung = energieversorgung;
		this.schutzschilde = schutzschilde;
		this.lebenserhaltungsysteme = lebenserhaltungsysteme;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.reparaturAndroiden = reparaturAndroiden;
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEnergieversorgung() {
		return energieversorgung;
	}

	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	public int getSchutzschilde() {
		return schutzschilde;
	}

	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}

	public int getLebenserhaltungsysteme() {
		return lebenserhaltungsysteme;
	}

	public void setLebenserhaltungsysteme(int lebenserhaltungsysteme) {
		this.lebenserhaltungsysteme = lebenserhaltungsysteme;
	}

	public int getHuelle() {
		return huelle;
	}

	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	public int getPhotonentorpedos() {
		return photonentorpedos;
	}

	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}

	public int getReparaturAndroiden() {
		return reparaturAndroiden;
	}

	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	/**
	 * Falls mindestens ein Photonentorpedo
	 * geladen ist, wird ein Photonentorpedo
	 * aus dem Bestand entfernt und das Ziel 
	 * erhält einen Treffer. Die Nachricht
	 * "Photonentorpedo abgeschossen" wird an 
	 * den Broadcastkommunikator gessendet. 
	 * Andernfalls wird die Nachricht
	 * "-=*Click*=-" an den Broadcastkommunikator 
	 * gesendet.
	 * @param ziel das Ziel das getroffen wird
	 */
	public void photonentorpedoSchießen(Raumschiff ziel) {
		if(photonentorpedos == 0) {
			broadcastSenden(broadcastKommunikator, "-=*Click*=-");
		}else {
			photonentorpedos -= 1;
			broadcastSenden(broadcastKommunikator, "Photonentorpedo abgeschossen");
			ziel.trefferErhalten(true);
		}
	}
	
	/**
	 * Ist die Energieversorgung größer gleich
	 * 50, wird die Energieversorgung um 50
	 * verringert und das Ziel erhält einen 
	 * Treffer. Die Nachricht "Phaserkanone 
	 * abgeschossen" wird an den Broadcast-
	 * kommunikator gesendet. Ansonsten wird die 
	 * Nachricht "-=*Click*=-" an den Broadcast-
	 * kommunikator gesendet.
	 * @param ziel das Ziel das getroffen wird
	 */
	public void phaserkanoneSchießen(Raumschiff ziel) {
		if(energieversorgung < 50) {
			broadcastSenden(broadcastKommunikator, "-=*Click*=-");
		}else {
			energieversorgung -= 50;
			broadcastSenden(broadcastKommunikator, "Phaserkanone abgeschossen");
			ziel.trefferErhalten(true);
		}
	}
	
	/**
	 * Falls getroffen true ist, wird die Nachricht
	 * "<Name> wurde getroffen" ausgegeben. Die
	 * Schutzschilde werden um 50 verringert. Sind die 
	 * Schutzschilde bei einem Wert unter Null,
	 * verringern sich die Werte der Energieversorgung
	 * und der Hülle um 50. Ist die Hülle unter Null,
	 * erhalten die Lebenserhaltungssysteme einen
	 * kritischen Schaden (-999) und die Nachricht
	 * "Lebenserhaltungssysteme von <Name> wurden vernichtet!"
	 * wird an den Broadcastkommunikator gesendet. 
	 * @param getroffen gibt an ob ein Treffer erhalten wurde
	 */
	private void trefferErhalten(boolean getroffen) {
		if(getroffen) {
			System.out.printf("%s wurde getroffen!\n",getName());
			schutzschilde -= 50;
			if(schutzschilde <= 0) {
				huelle -= 50;
				energieversorgung -= 50;
				if(huelle <= 0) {
					lebenserhaltungsysteme -= 999;
					broadcastSenden(broadcastKommunikator, "Lebenserhaltungssysteme von " + getName() + 
							                               "wurden vernichtet!");
				}
			}
		}
	}
	
	/**
	 * Fügt eine Nachricht dem Broadcastkommunikator
	 * hinzu.
	 * @param broadcastKommunikator der Broadcastkommunikator, 
	 * 		  der die Nachricht enthält
	 * @param nachricht die Nachricht die an den Broadcast-
	 * 		  kommunikator gesendet wird.
	 */
	public void broadcastSenden(ArrayList<String> broadcastKommunikator, String nachricht){
		broadcastKommunikator.add(nachricht);
	}

	/**
	 * Fügt dem Raumschiff eine neue Ladung hinzu
	 * @param neueLadung die Ladung die hinzugefügt wird.
	 */
	public void ladungHinzufuegen(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Gibt den Broadcastkommunikator zurück
	 * der die Nachrichten aus der broadcast-
	 * senden-Methode enthält.
	 * @return der Broadcastkommunikator
	 */
	public ArrayList<String> zeigeLogbuch(){
		return broadcastKommunikator;
	}
	
	/**
	 * Lädt Photonentorpedos aus der Ladung in das Raumschiff,
	 * sofern welche in der Ladung vorhanden sind. Durchsucht
	 * die Ladung nach Photonentorpedos. Sind keine vorhanden,
	 * wird die Nachricht "-=*Click*=-" an den Broadcast-
	 * kommunikator gesendet, anderfalls wird geprüft, ob die 
	 * angeforderte Anzahl Photonentorpedos in der Ladung vor-
	 * handen ist. Falls ja, wird die Anzahl der geforderten 
	 * Photonentorpedos von dem Ladungsbestand abgezogen, und 
	 * die Nachricht "<anzahlPhotonentorpedos> wurde eingesetzt"
	 * wird ausgegeben. Im anderen Fall werden alle Photonentor-
	 * pedos aus der Ladung ins Raumschiff geladen, 
	 * <anzahlPhotonentorpedos> wird entsprechend angepasst.
	 * Eine Nachricht entsprechend dem anderen Fall wird ausge-
	 * geben. 
	 * @param anzahlPhotonentorpedos angeforderte Anzahl an
	 * 								 Photonentorpedos.
	 */
	public void ladePhotonentorpedos(int anzahlPhotonentorpedos) {
		boolean photonentorpedosEnthalten = false;
		int photonentorpedo = -1;
		ListIterator<Ladung> li = ladungsverzeichnis.listIterator();
		while(li.hasNext()) {
			Ladung la = li.next();
			if(la.getName().equals("Photonentorpedo")) {
				photonentorpedosEnthalten = true;
				photonentorpedo = li.previousIndex();
			}
		}
		if(photonentorpedosEnthalten == false) {
			System.out.println("Keine Photonentorpedos gefunden!");
			broadcastSenden(broadcastKommunikator, "-=*Click*=-");
		}else {
			if(anzahlPhotonentorpedos > ladungsverzeichnis.get(photonentorpedo).getAnzahl()) {
				photonentorpedos += ladungsverzeichnis.get(photonentorpedo).getAnzahl();
				System.out.println(ladungsverzeichnis.get(photonentorpedo).getAnzahl() + 
														" Photonentorpedo(s) eingesetzt");
				ladungsverzeichnis.get(photonentorpedo).setAnzahl(0);
			}else {
				photonentorpedos += anzahlPhotonentorpedos;
				ladungsverzeichnis.get(photonentorpedo).setAnzahl(ladungsverzeichnis.get(photonentorpedo).
																	getAnzahl() - anzahlPhotonentorpedos);
				System.out.println(anzahlPhotonentorpedos + " Photonentorpedo(s) eingesetzt");
			}
		}
	}
	
	/**
	 * Sendet einen Reparaturauftrag an die Reparatur-
	 * androiden. Es können bis zu drei Systeme zur
	 * Reparatur ausgewählt werden. Je nach Anzahl vor-
	 * handener Reparaturandroiden und ausgewählter
	 * Systeme vergrößert sich der Wert, um den das oder
	 * die Systeme erhöht werden. Eine zufallszahl wird 
	 * in der Berechnung des Wertes verwendet. 
	 * @param energieversorgung gibt an ob dieses System
	 *                          repariert werden soll
	 * @param schutzschilde gibt an ob dieses System
	 *                          repariert werden soll
	 * @param huelle gibt an ob dieses System repariert 
	 * 				 werden soll
	 * @param anzahlReparaturAndroiden gibt an ob dieses 
	 * 								   System repariert 
	 * 								   werden soll
	 */
	public void sendeReparaturauftrag(boolean energieversorgung, 
									  boolean schutzschilde, 
									  boolean huelle, 
									  int anzahlReparaturAndroiden) {
		Random r = new Random();
		int zufallszahl = r.nextInt(101);
		int anzahlStrukturen = 0;
		
		if(energieversorgung)
			anzahlStrukturen++;
		if(schutzschilde)
			anzahlStrukturen++;
		if(huelle)
			anzahlStrukturen++;
		
		if(reparaturAndroiden < anzahlReparaturAndroiden)
			anzahlReparaturAndroiden = reparaturAndroiden;
		
		if(energieversorgung) 
			this.energieversorgung += zufallszahl*anzahlReparaturAndroiden/anzahlStrukturen;
		if(schutzschilde)
			this.schutzschilde += zufallszahl*anzahlReparaturAndroiden/anzahlStrukturen;
		if(huelle)
			this.huelle += zufallszahl*anzahlReparaturAndroiden/anzahlStrukturen;
	}
	
	/**
	 * Gibt Nachricht über den Zustand des Raumschiffs aus.
	 * Enthalten sind Name, Energieversorgung, Schutzschilde,
	 * Hülle, Kebenserhaltungssysteme, Anzahl Photonentorpedos
	 * und Anzahl Reparaturandroiden.
	 */
	public void zeigeZustand() {
		System.out.printf(">   Zustand des Raumschiffs   <\n" +
						  "Name:%26s\n" +
						  "Energieversorgung:%13d\n" +
						  "Schutzschilde:%17d\n" +
						  "Lebenserhaltungssysteme:%7d\n" +
						  "Hülle:%25d\n" +
						  "Photonentorpedos:%14d\n" + 
						  "Reparatur-Androiden:%11d\n",
						  name,energieversorgung,schutzschilde,
						  lebenserhaltungsysteme,huelle,
						  photonentorpedos,reparaturAndroiden);
	}

	/**
	 * Gibt das Ladungsverzeichnis aus. Iteriert durch
	 * das Verzeichnes und gibt jeden Name der jeweiligen 
	 * Ladung mitsamt der vorhandenen Anzahl aus.
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println(">          Ladungen           <");
		ListIterator<Ladung> li = ladungsverzeichnis.listIterator();
		
		while(li.hasNext()) {
			Ladung ladung = li.next();
			System.out.printf("%-28s%3d\n",ladung.getName(), ladung.getAnzahl());
		}
	}
	
	/**
	 * Räumt das Ladungsverzeichnes auf. Iteriert durch das 
	 * Verzeichnis und entfernt jeden Eintrag, bei dem die 
	 * Anzahl Null (oder kleiner) ist.
	 */
	public void ladungsverzeichnisAufraeumen() {
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			if(ladungsverzeichnis.get(i).getAnzahl() <= 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
}
