import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Lernsituation Raumschiffe - Klasse Tester
 * - beinhaltet die Main-Methode 
 * @author LeocadioMaungue
 * @version 1.0.0.0
 * 
 */
public class Tester {

	public static void main(String[] args) {
ArrayList<String> broadcastKommunikator = new ArrayList<>();
		
		Raumschiff klingonen = new Raumschiff("IKS Heght'ta", 100, 100, 100, 100, 1, 2, broadcastKommunikator);
		klingonen.ladungHinzufuegen(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.ladungHinzufuegen(new Ladung("Bat'leth Klingonen Schwert", 200));
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2, broadcastKommunikator);
		romulaner.ladungHinzufuegen(new Ladung("Borg-Schrott", 5));
		romulaner.ladungHinzufuegen(new Ladung("Rote Materie", 2));
		romulaner.ladungHinzufuegen(new Ladung("Plasma-Waffe", 50));
		
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5, broadcastKommunikator);
		vulkanier.ladungHinzufuegen(new Ladung("Forschungssonde",35));
		vulkanier.ladungHinzufuegen(new Ladung("Photonentorpedo", 3));
		
		klingonen.photonentorpedoSchießen(romulaner);
		romulaner.phaserkanoneSchießen(klingonen);
		vulkanier.broadcastSenden(broadcastKommunikator, "Gewalt ist nicht logisch");
		
		klingonen.zeigeZustand();
		klingonen.ladungsverzeichnisAusgeben();
		
		vulkanier.sendeReparaturauftrag(true, true, true, 5);
		vulkanier.ladePhotonentorpedos(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		
		klingonen.photonentorpedoSchießen(romulaner);
		klingonen.photonentorpedoSchießen(romulaner);
				
		klingonen.zeigeZustand();
		romulaner.zeigeZustand();
		vulkanier.zeigeZustand();
		
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.ladungsverzeichnisAusgeben();
		
		broadcastKommunikator = vulkanier.zeigeLogbuch();
		
		ListIterator<String> li = broadcastKommunikator.listIterator();
		
		while(li.hasNext()) {
			System.out.println(li.next());
		}
	}
}
